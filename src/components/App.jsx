import React from 'react';
// import Settings from './settings/Settings';

/**
 * Función Anónima para crear un Componente principal
 * @returns {React.Component} Componente principal de nuestra aplicación
 */
const App = () => {
    return (
      <div>
        <h1>Proyecto Final React JS</h1>
      </div>
    );
};

export default App;
